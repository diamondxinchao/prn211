﻿using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.IService
{
    public interface IExpensesService
    {
        Task<IEnumerable<Expense>> GetAllExpense();
        Task<Expense> GetExpense(int expenseId);
    }
}
