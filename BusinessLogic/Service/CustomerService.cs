using BusinessLogic.IService;
using BusinessLogic.UnitOfWork;

namespace BusinessLogic.Service;

public class CustomerService : ICustomerService
{
    private readonly IUnitOfWork _unitOfWork;

    public CustomerService(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }
}