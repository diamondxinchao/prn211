﻿using BusinessLogic.IService;
using BusinessLogic.UnitOfWork;
using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Service
{
    public class ExpensesService : IExpensesService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ExpensesService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Expense>> GetAllExpense()
        {
            var expenseDetail = _unitOfWork.ExpenseRepository.GetAllEntity();
            return expenseDetail;
        }

        public Task<Expense> GetExpense(int expenseId)
        {
            throw new NotImplementedException();
        }
    }
}
