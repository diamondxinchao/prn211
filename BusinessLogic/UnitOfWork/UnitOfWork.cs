using DataAccess.IRepository;
using DataAccess.Models;
using DataAccess.Repository;
using DataAccess.Repository.IRepository;

namespace BusinessLogic.UnitOfWork;

public class UnitOfWork : IUnitOfWork
{
    private readonly BirdCageManagementsContext _context;
    private readonly ICustomerRepository _customerRepository;
    private readonly IExpenseRepository _expenseRepository;
    private readonly IInventoryRepository _inventoryRepository;
    private readonly IMaterialRepository _materialRepository;
    private readonly IOrderItemRepository _orderItemRepository;
    private readonly IOrderRepository _orderRepository;
    private readonly IProductRepository _productRepository;
    private readonly IStaffRepository _staffRepository;
    private readonly IWorkOnRepository _workOnRepository;

    public UnitOfWork(BirdCageManagementsContext context)
    {
        _context = context;
        _customerRepository = new CustomerRepository(context);
        _expenseRepository = new ExpenseRepository(context);
        _inventoryRepository = new InventoryRepository(context);
        _materialRepository = new MaterialRepository(context);
        _orderItemRepository = new OrderItemRepository(context);
        _orderRepository = new OrderRepository(context);
        _productRepository = new ProductRepository(context);
        _staffRepository = new StaffRepository(context);
        _workOnRepository = new WorkOnRepository(context);
    }

    public ICustomerRepository CustomerRepository => _customerRepository;

    public IExpenseRepository ExpenseRepository => _expenseRepository;

    public IInventoryRepository InventoryRepository => _inventoryRepository;

    public IMaterialRepository MaterialRepository => _materialRepository;

    public IOrderItemRepository OrderItemRepository => _orderItemRepository;

    public IOrderRepository OrderRepository => _orderRepository;

    public IProductRepository ProductRepository => _productRepository;

    public IStaffRepository StaffRepository => _staffRepository;

    public IWorkOnRepository WorkOnRepository => _workOnRepository;

    public async Task<int> SaveChange() => await _context.SaveChangesAsync();
}