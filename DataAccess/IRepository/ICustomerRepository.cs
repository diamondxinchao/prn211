using DataAccess.Models;

namespace DataAccess.Repository.IRepository;

public interface ICustomerRepository : IGenericRepository<Customer>
{
    
}