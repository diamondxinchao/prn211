﻿

using DataAccess.Models;
using DataAccess.Repository.IRepository;

namespace DataAccess.IRepository
{
    public interface IExpenseRepository : IGenericRepository<Expense>
    {
    }
}
