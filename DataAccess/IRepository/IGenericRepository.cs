namespace DataAccess.Repository.IRepository;

public interface IGenericRepository<T> where T: class
{
    public List<T> GetAllEntity();
    public T GetEntityById(int entityId);
    public void AddNewEntity(T entity);
    public void UpdateEntity(T entity);
    public void DeleteEntity(int entityId);
}