using DataAccess.Models;
using DataAccess.Repository.IRepository;

namespace DataAccess.Repository;

public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
{
    public CustomerRepository(BirdCageManagementsContext context) : base(context)
    {
    }
    
}