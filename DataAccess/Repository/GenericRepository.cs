using DataAccess.Models;
using DataAccess.Repository.IRepository;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repository;

public class GenericRepository<T> : IGenericRepository<T> where T:class
{
    protected readonly BirdCageManagementsContext _context;
    protected readonly DbSet<T> _dbSet;

    public GenericRepository(BirdCageManagementsContext context)
    {
        _context = context;
        _dbSet = context.Set<T>();
    }
    public List<T> GetAllEntity()
    {
        return _dbSet.ToList();
    }

    public T GetEntityById(int entityId)
    {
        return _dbSet.Find(entityId);
    }

    public void AddNewEntity(T entity)
    {
        _dbSet.Add(entity);
    }

    public void UpdateEntity(T entity)
    {
        _dbSet.Attach(entity);
        _context.Entry(entity).State = EntityState.Modified;
    }

    public void DeleteEntity(int entityId)
    {
        var entity = GetEntityById(entityId);
        _dbSet.Remove(entity);
    }
}